﻿module method
use matrixOperations
implicit none

    contains

    subroutine approx(XYP,XY)
    real(8), dimension(1:,0:), intent(in) :: XYP
    !real(8), parameter :: n = size(XYP,dim=2)-1    !возможно ли сделать тут новую локальную константу?
    real(8), dimension(0:size(XYP,dim=2)-1) :: X, Y, P, S, R !приходится избегать глобализации переменной n таким образом
    real(8), dimension(2,0:100*(size(XYP,dim=2)-1)), intent(out) :: XY
    real(8), dimension(-1:1,0:size(XYP,dim=2)-1) :: A, B, Q
        call cutOut(X,Y,P,XYP)
        call ABQbuilding(X,P,A,B,Q)
        call matrix(A,B,Q,Y,S,R)
        call spline(X,S,R,XY)
        
    end subroutine approx
    
    subroutine cutOut(X,Y,P,XYP)
    real(8), dimension(1:,0:), intent(in) :: XYP
    real(8), dimension(0:size(XYP,dim=2)-1) :: X, Y, P
    real(8), dimension(-1:1,0:size(XYP,dim=2)-1) :: A, B, Q
    integer(4) :: i, n

    n=size(XYP,dim=2)-1

    forall (i=0:n)
        X(i)=XYP(1,i)
        Y(i)=XYP(2,i)
        P(i)=XYP(3,i)
    end forall
    end subroutine cutOut
    
    subroutine ABQbuilding(X,P,A,B,Q)
    real(8), dimension(0:), intent(in) :: X, P
    real(8), dimension(-1:1,0:size(X)-1), intent(out) :: A, B, Q
    integer(4) :: i, n

    n=size(X)-1

    A(0,0)=2*(X(1)-X(0)); A(-1,0)=0.0; A(1,0)=0.0
    A(-1,1)=0; A(0,1)=2*(X(2)-X(0)); A(1,1)=X(2)-X(1)
   
    forall (i=2:n-2)
        A(-1,i)=X(i)-X(i-1)
        A(0,i)=2*(X(i+1)-X(i-1))
        A(1,i)=X(i+1)-X(i)
    end forall

    A(-1,n-1)=X(n-1)-X(n-2); A(0,n-1)=2*(X(n)-X(n-2)); A(1,n-1)=0
    A(0,n)=2*(X(n)-X(n-1)); A(-1,n)=0; A(1,n)=0
    
    B(-1:1,0)=0
    
    forall (i=1:n-1)
        B(-1,i)=1.0/(X(i)-X(i-1))
        B(0,i)=-( 1.0/(X(i)-X(i-1))+1.0/(X(i+1)-X(i)) )
        B(1,i)=1.0/(X(i+1)-X(i))
    end forall
    
    B(-1:1,n)=0

    forall (i=0:n)
        Q(0,i)=1/P(i); Q(-1,i)=0; Q(1,i)=0
    end forall

    end subroutine ABQbuilding

    
    subroutine matrix(A,B,Q,Y,S,R)
    real(8), dimension(-1:,0:), intent(in) :: A, B, Q
    real(8), dimension(0:), intent(in) :: Y
    real(8), dimension(0:size(Y)-1), intent(out) :: S, R
    real(8), dimension(0:size(Y)-1) :: BY, QBtS
    real(8), dimension(-2:2,0:size(Y)-1) :: BQ5d, BQBt
    real(8), dimension(-1:1,0:size(Y)-1) :: Bt, QBt
    integer(4) :: i, n
    
    n=size(Y)-1

    call transposition(B,Bt,n)
    
    call TriMatrixMultiplication(Q,Bt,BQ5d)

    forall (i=0:n) QBt(-1:1,i)=BQ5d(-1:1,i)
    
    call TriMatrixMultiplication(B,QBt,BQBt)

    BQBt=BQBt*6
    BQBt(-1:1,:)=BQBt(-1:1,:)+A(-1:1,:) ! Не буду ради этой операции создавать массив
    call matrixVector(B,Y,BY)
    BY=BY*6
    call pointSwipe(BQBt,BY,S)
    call matrixVector(QBt,S,QBtS)
    R=Y-QBtS
    
    end subroutine matrix
    
    subroutine transposition(B,Bt,n)
    real(8), dimension(-1:,0:), intent(in) :: B
    integer(4), intent(in) :: n
    real(8), dimension(-1:1,0:n), intent(out) :: Bt
    integer(4) :: i
    
    Bt(-1,0)=0; Bt(0,0)=0; Bt(1,0)=B(-1,1)
    forall (i=1:n-1)
        Bt(-1,i)=B(1,i-1)
        Bt(0,i)=B(0,i)
        Bt(1,i)=B(-1,i+1)
    end forall
    Bt(-1,n)=B(1,n-1); Bt(0,n)=0; Bt(1,n)=0
    
    end subroutine transposition
    
    subroutine pointSwipe(array,d,X)
    real(8), dimension(-2:,1:), intent(in) :: array
    real(8), dimension(1:), intent(in) :: d
    real(8), dimension(1:size(d)), intent(out) :: X
    real(8), dimension(1:size(d)) :: b, a, p, q, r ! См. в задание, для чего нужны эти массивы
    integer :: i, n

    n=size(d)

    b(1)=0
    a(1)=array(0,1)
    p(1)=array(1,1)/a(1)
    q(1)=array(2,1)/a(1)
    r(1)=d(1)/a(1)

    b(2)=array(-1,2)
    a(2)=array(0,2)-p(1)*b(2)
    p(2)=(array(1,2)-q(1)*b(2))/a(2)
    q(2)=array(2,2)/a(2)
    r(2)=(d(2)-r(1)*b(2))/a(2)

    do i=3,n-2
        b(i)=array(1,i-1)-p(i-2)*array(2,i-2)
        a(i)=array(0,i)-p(i-1)*b(i)-q(i-2)*array(2,i-2)
        p(i)=(array(1,i)-q(i-1)*b(i))/a(i)
        q(i)=array(2,i)/a(i)
        r(i)=(d(i)-r(i-1)*b(i)-r(i-2)*array(2,i-2))/a(i)
    enddo

    b(n-1)=array(1,n-2)-p(n-3)*array(2,n-3)
    a(n-1)=array(0,n-1)-p(n-2)*b(n-1)-q(n-3)*array(2,n-3)
    p(n-1)=(array(1,n-1)-q(n-2)*b(n-1))/a(n-1)
    q(n-1)=0
    r(n-1)=(d(n-1)-r(n-2)*b(n-1)-r(n-3)*array(2,n-3))/a(n-1)

    b(n)=array(1,n-1)-p(n-2)*array(2,n-2)
    a(n)=array(0,n)-p(n-1)*b(n)-q(n-2)*array(2,n-2)
    p(n)=-q(n-1)*b(n)/a(n)
    q(n)=0
    r(n)=(d(n)-r(n-1)*b(n)-r(n-2)*array(2,n-2))/a(n)

    X(n)=r(n)
    X(n-1)=r(n-1)-p(n-1)*X(n)
    do i=n-2,1,-1
        X(i)=r(i)-p(i)*X(i+1)-q(i)*X(i+2)
    enddo

    end subroutine pointSwipe

    subroutine spline(X,S,R,XY)
    real(8), dimension(0:), intent(in):: X, S, R
    real(8), dimension(0:size(X)-1) :: Xmodified
    real(8), dimension(1:2,0:100*(size(X)-1)), intent(out) :: XY
    real(8) :: h, t
    integer(4), dimension(1) :: nmax
    integer(4) :: i, j, k, n

    n=size(R)-1
    Xmodified=X
    forall (i=0:100*n) XY(1,i)=X(0)+i*(X(n)-X(0))/(100*n)

    do i=0,100*n-1
        do k=0,size(X)-1
            if (Xmodified(k)<=XY(1,i)) then
                Xmodified(k)=Xmodified(n)+1.0
            endif    
        enddo
        nmax=minloc(Xmodified)-2
        j=nmax(1)
        h=X(j+1)-X(j)
        t=(XY(1,i)-X(j))/h
        XY(2,i)=R(j)*(1-t)+R(j+1)*t-h**2*t*(1-t)/6.0*((2.0-t)*S(j)+(1.0+t)*S(j+1))
    enddo
    h=X(n)-X(n-1)
    XY(2,100*n)=R(j+1)

    end subroutine spline
    
end module method