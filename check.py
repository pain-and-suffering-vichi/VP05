import pandas as pd
import matplotlib.pyplot as plt
from io import StringIO
from numpy import *

path1='../VP_05/data.dat'
path2='../VP_05/result.dat'
x1, y1 = loadtxt(path1,unpack=True, usecols=[0,1])
x, y = loadtxt(path2,unpack=True, usecols=[0,1])

plt.figure()
plt.errorbar(x1, y1, fmt='o-', color='red')
plt.errorbar(x, y, fmt='o-', color='blue')
plt.show()