﻿ program VP_05
!  VP_05.f90 
!
!  FUNCTIONS:
!  VP_05 Аппроксимирует кривую сплайнами.
!
    use method
    implicit none
   
    
    real(8), allocatable :: XYP(:,:), XY(:,:)
    integer(4) :: i, n

    open(1,file='data.dat')
        read(1,'(2x,i8)') n
        allocate(XYP(3,0:n), XY(2,0:100*n)) !Здесь чисто технически более удобно, чтобы по более длиному измерению массив начинался с 0
        do i=0, n
	        read(1,*) XYP(1:3,i)
        enddo
    close(1)
    call approx(XYP,XY)

    deallocate(XYP)
    open(2, file='result.dat')
        do i=0,100*n
            write(2,*) XY(1:2,i)
        enddo
    close(2)
    deallocate(XY)


    end program VP_05

